export interface Payment {
  paymentSystem:string,
  paymentType:string,
  merchantAccountId:string,
  salt:string,
  key:string,
  username:string,
  password:string,
  enableDelivery:boolean,
  enablePickUp:boolean,
  tableReservation:boolean
}
