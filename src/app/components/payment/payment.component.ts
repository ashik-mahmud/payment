import { Component, OnInit } from '@angular/core';
import {AngularFirestore} from "angularfire2/firestore";
import {Payment} from "../../models/payment";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  paymentType = [
    {id:1, name:"Paypal"},
    {id:2, name:"Stripe"},
    {id:3, name:"Cash"}
    ];
  payment:Payment = {
    paymentSystem:'',
    paymentType:'',
    merchantAccountId:'',
    salt:'',
    key:'',
    username:'',
    password:'',
    enableDelivery:false,
    enablePickUp:false,
    tableReservation:false
  };

  constructor(private db:AngularFirestore) { }

  ngOnInit() {
  }
  onSubmit(form:NgForm) {
    //save the data to the firebase
    this.db.collection('payment').add(this.payment);

    //reset the form
    form.reset();
    this.payment.enableDelivery = false;
    this.payment.enablePickUp = false;
    this.payment.tableReservation = false;
  }

}
