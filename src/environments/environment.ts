// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  config : {
    apiKey: "AIzaSyAEHUPhGIujdQ5o_WFD8Iu4LC0hQ0o1XuU",
    authDomain: "payment-b2c60.firebaseapp.com",
    databaseURL: "https://payment-b2c60.firebaseio.com",
    projectId: "payment-b2c60",
    storageBucket: "payment-b2c60.appspot.com",
    messagingSenderId: "939352426817"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
